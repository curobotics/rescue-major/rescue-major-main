EESchema Schematic File Version 4
LIBS:test1-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 9000 4150 0    50   ~ 0
29
Text Label 9000 4050 0    50   ~ 0
30
Text Label 9000 3950 0    50   ~ 0
31
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 5E13D66F
P 4400 1300
F 0 "J5" H 4508 1481 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4508 1390 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx02_1x02_P2.50mm_Vertical" H 4400 1300 50  0001 C CNN
F 3 "~" H 4400 1300 50  0001 C CNN
	1    4400 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3800 5250 3300
Connection ~ 5250 3800
Wire Wire Line
	4450 3800 5250 3800
Wire Wire Line
	5250 3300 5250 2800
Connection ~ 5250 3300
Wire Wire Line
	4450 3300 5250 3300
Connection ~ 5250 4300
Wire Wire Line
	5250 2800 4450 2800
Wire Wire Line
	5250 4300 5250 3800
Wire Wire Line
	5250 4300 4450 4300
$Comp
L power:+5V #PWR04
U 1 1 5E134845
P 4800 2250
F 0 "#PWR04" H 4800 2100 50  0001 C CNN
F 1 "+5V" V 4815 2378 50  0000 L CNN
F 2 "" H 4800 2250 50  0001 C CNN
F 3 "" H 4800 2250 50  0001 C CNN
	1    4800 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 2700 4800 3200
Connection ~ 4800 2700
Wire Wire Line
	4450 2700 4800 2700
Wire Wire Line
	4800 3200 4800 3700
Connection ~ 4800 3200
Wire Wire Line
	4450 3200 4800 3200
Wire Wire Line
	4800 3700 4800 4200
Connection ~ 4800 3700
Wire Wire Line
	4450 3700 4800 3700
Wire Wire Line
	4800 2250 4800 2700
Wire Wire Line
	4800 4200 4450 4200
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 5E12F6EB
P 4250 4200
F 0 "J4" H 4358 4481 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4358 4390 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 4250 4200 50  0001 C CNN
F 3 "~" H 4250 4200 50  0001 C CNN
	1    4250 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5E12E3AB
P 4250 3700
F 0 "J3" H 4358 3981 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4358 3890 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 4250 3700 50  0001 C CNN
F 3 "~" H 4250 3700 50  0001 C CNN
	1    4250 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5E12D556
P 4250 3200
F 0 "J2" H 4358 3481 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4358 3390 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 4250 3200 50  0001 C CNN
F 3 "~" H 4250 3200 50  0001 C CNN
	1    4250 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5E12BBDF
P 4250 2700
F 0 "J1" H 4358 2981 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4358 2890 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 4250 2700 50  0001 C CNN
F 3 "~" H 4250 2700 50  0001 C CNN
	1    4250 2700
	1    0    0    -1  
$EndComp
Connection ~ 6450 4800
$Comp
L power:+12V #PWR01
U 1 1 5E118E4A
P 6450 4800
F 0 "#PWR01" H 6450 4650 50  0001 C CNN
F 1 "+12V" V 6465 4928 50  0000 L CNN
F 2 "" H 6450 4800 50  0001 C CNN
F 3 "" H 6450 4800 50  0001 C CNN
	1    6450 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7950 4800 8250 4800
Wire Wire Line
	6650 4800 7150 4800
Wire Wire Line
	7550 5100 8250 5100
Connection ~ 7550 5100
$Comp
L power:GND #PWR03
U 1 1 5E11651A
P 7550 5100
F 0 "#PWR03" H 7550 4850 50  0001 C CNN
F 1 "GND" H 7555 4927 50  0000 C CNN
F 2 "" H 7550 5100 50  0001 C CNN
F 3 "" H 7550 5100 50  0001 C CNN
	1    7550 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 5100 7550 5100
$Comp
L Regulator_Switching:TSR_1-2450 U1
U 1 1 5E11552C
P 7550 4900
F 0 "U1" H 7550 5267 50  0000 C CNN
F 1 "TSR_1-2450" H 7550 5176 50  0000 C CNN
F 2 "TSR_1-2450:CONV_TSR_1-2450" H 7550 4750 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 7550 4900 50  0001 C CNN
	1    7550 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 12V1
U 1 1 5D744D9C
P 4600 1950
F 0 "12V1" H 4680 1942 50  0000 L CNN
F 1 "Conn_01x02" H 4680 1851 50  0000 L CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx02_1x02_P2.50mm_Vertical" H 4600 1950 50  0001 C CNN
F 3 "~" H 4600 1950 50  0001 C CNN
	1    4600 1950
	-1   0    0    1   
$EndComp
Connection ~ 8450 4800
Wire Wire Line
	8450 4800 8650 4800
Wire Wire Line
	8250 4800 8450 4800
Connection ~ 8250 5100
Wire Wire Line
	8250 5100 8450 5100
$Comp
L Device:C C_REG_OUT_B1
U 1 1 5D7487DD
P 8450 4950
F 0 "C_REG_OUT_B1" H 8565 4996 50  0000 L CNN
F 1 "10uF" H 8565 4905 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 8488 4800 50  0001 C CNN
F 3 "~" H 8450 4950 50  0001 C CNN
	1    8450 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4800 6650 4800
Connection ~ 6650 5100
Wire Wire Line
	6650 5100 6450 5100
$Comp
L Device:C C_REG_IN_B1
U 1 1 5D734F0E
P 6450 4950
F 0 "C_REG_IN_B1" H 5900 5000 50  0000 L CNN
F 1 "10uF" H 6150 4900 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 6488 4800 50  0001 C CNN
F 3 "~" H 6450 4950 50  0001 C CNN
	1    6450 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR08
U 1 1 5D746966
P 4800 1850
F 0 "#PWR08" H 4800 1700 50  0001 C CNN
F 1 "+12V" V 4800 2000 50  0000 L CNN
F 2 "" H 4800 1850 50  0001 C CNN
F 3 "" H 4800 1850 50  0001 C CNN
	1    4800 1850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5D7460C5
P 4800 1950
F 0 "#PWR09" H 4800 1700 50  0001 C CNN
F 1 "GND" V 4805 1822 50  0000 R CNN
F 2 "" H 4800 1950 50  0001 C CNN
F 3 "" H 4800 1950 50  0001 C CNN
	1    4800 1950
	0    -1   -1   0   
$EndComp
Connection ~ 8250 4800
$Comp
L Device:C C_REG_OUT_S1
U 1 1 5D68CFEC
P 8250 4950
F 0 "C_REG_OUT_S1" H 8000 5250 50  0000 L CNN
F 1 "10nF" H 8000 4850 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 8288 4800 50  0001 C CNN
F 3 "~" H 8250 4950 50  0001 C CNN
	1    8250 4950
	1    0    0    -1  
$EndComp
Connection ~ 6650 4800
$Comp
L Device:C C_REG_IN_S1
U 1 1 5D68C59D
P 6650 4950
F 0 "C_REG_IN_S1" H 6450 5200 50  0000 L CNN
F 1 "10nF" H 6750 4900 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 6688 4800 50  0001 C CNN
F 3 "~" H 6650 4950 50  0001 C CNN
	1    6650 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5D689AAB
P 8650 4800
F 0 "#PWR02" H 8650 4650 50  0001 C CNN
F 1 "+5V" V 8665 4928 50  0000 L CNN
F 2 "" H 8650 4800 50  0001 C CNN
F 3 "" H 8650 4800 50  0001 C CNN
	1    8650 4800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5E11B093
P 6300 1450
F 0 "#PWR05" H 6300 1200 50  0001 C CNN
F 1 "GND" H 6305 1277 50  0000 C CNN
F 2 "" H 6300 1450 50  0001 C CNN
F 3 "" H 6300 1450 50  0001 C CNN
	1    6300 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 1450 6550 1450
Wire Wire Line
	6300 2250 6550 2250
Text Label 4900 2600 0    50   ~ 0
PWM1
Wire Wire Line
	4450 4100 4900 4100
Wire Wire Line
	4450 3600 4900 3600
Text Label 4900 4100 0    50   ~ 0
PWM4
Text Label 4900 3600 0    50   ~ 0
PWM3
Wire Wire Line
	4450 3100 4900 3100
Wire Wire Line
	4450 2600 4900 2600
Text Label 4900 3100 0    50   ~ 0
PWM2
Wire Wire Line
	6200 2050 6550 2050
Text Label 6200 2050 0    50   ~ 0
PWM4
Text Label 6050 1950 0    50   ~ 0
PWM3
Text Label 5900 1850 0    50   ~ 0
PWM2
Text Label 5750 1750 0    50   ~ 0
PWM1
Wire Wire Line
	4600 1300 5050 1300
Text Label 5050 1300 0    50   ~ 0
RX
Wire Wire Line
	6300 2350 6550 2350
Text Label 6300 2350 0    50   ~ 0
TX
Text Label 6300 2250 0    50   ~ 0
RX
Wire Wire Line
	8750 3950 9000 3950
Wire Wire Line
	8750 4050 9000 4050
Wire Wire Line
	8750 4150 9000 4150
Text Label 6300 1650 2    50   ~ 0
3
Text Label 6300 1550 2    50   ~ 0
2
Text Label 6300 2150 2    50   ~ 0
8
Text Label 6300 2450 2    50   ~ 0
11
Text Label 6300 2550 2    50   ~ 0
12
Text Label 6300 2650 2    50   ~ 0
13
Text Label 6300 2750 2    50   ~ 0
14
Text Label 6300 2850 2    50   ~ 0
15
Text Label 6300 2950 2    50   ~ 0
16
Text Label 6300 3050 2    50   ~ 0
17
Text Label 6300 3150 2    50   ~ 0
18
Text Label 6300 3250 2    50   ~ 0
19
Text Label 6300 3350 2    50   ~ 0
20
Text Label 6100 3450 2    50   ~ 0
Analog_1
Text Label 6100 3550 2    50   ~ 0
Analog_2
Text Label 6300 3650 2    50   ~ 0
23
Text Label 6300 3750 2    50   ~ 0
24
Text Label 6300 3850 2    50   ~ 0
25
Text Label 6300 3950 2    50   ~ 0
26
Text Label 6300 4050 2    50   ~ 0
27
Text Label 6300 4150 2    50   ~ 0
28
Wire Wire Line
	6550 4150 6300 4150
Wire Wire Line
	6550 4050 6300 4050
Wire Wire Line
	6550 3950 6300 3950
Wire Wire Line
	6550 3850 6300 3850
Wire Wire Line
	6550 3750 6300 3750
Wire Wire Line
	6550 3650 6300 3650
Wire Wire Line
	6550 3350 6300 3350
Wire Wire Line
	6550 3250 6300 3250
Wire Wire Line
	6550 3150 6300 3150
Wire Wire Line
	6550 3050 6300 3050
Wire Wire Line
	6550 2950 6300 2950
Wire Wire Line
	6550 2850 6300 2850
Wire Wire Line
	6550 2750 6300 2750
Wire Wire Line
	6550 2650 6300 2650
Wire Wire Line
	6550 2550 6300 2550
Wire Wire Line
	6550 2450 6300 2450
Wire Wire Line
	6550 1650 6300 1650
Wire Wire Line
	6550 1550 6300 1550
Wire Wire Line
	6050 1950 6550 1950
Wire Wire Line
	6550 1850 5900 1850
Wire Wire Line
	6550 2150 6300 2150
Wire Wire Line
	6550 1750 5750 1750
Text Label 3400 2400 0    50   ~ 0
19
Text Label 3400 2300 0    50   ~ 0
20
Text Label 3400 2700 0    50   ~ 0
16
Text Label 3400 2600 0    50   ~ 0
17
Text Label 3400 2500 0    50   ~ 0
18
Text Label 3400 3000 0    50   ~ 0
13
Text Label 3400 2900 0    50   ~ 0
14
Text Label 3400 2800 0    50   ~ 0
15
Text Label 3400 3200 0    50   ~ 0
11
Text Label 3400 3100 0    50   ~ 0
12
Wire Wire Line
	6100 3450 6550 3450
Wire Wire Line
	6100 3550 6550 3550
Wire Wire Line
	4450 5300 5250 5300
Wire Wire Line
	4450 4800 5250 4800
Wire Wire Line
	4450 4700 4800 4700
Wire Wire Line
	4450 5200 4800 5200
$Comp
L Connector:Conn_01x03_Male J42
U 1 1 5E3B0ED1
P 4250 5200
F 0 "J42" H 4358 5481 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4358 5390 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 4250 5200 50  0001 C CNN
F 3 "~" H 4250 5200 50  0001 C CNN
	1    4250 5200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J38
U 1 1 5E3B0ED7
P 4250 4700
F 0 "J38" H 4358 4981 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4358 4890 50  0000 C CNN
F 2 "Connector_Molex2:Molex_Nano-Fit_105309-xx03_1x03_P2.50mm_Vertical" H 4250 4700 50  0001 C CNN
F 3 "~" H 4250 4700 50  0001 C CNN
	1    4250 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5100 4900 5100
Wire Wire Line
	4450 4600 4900 4600
$Comp
L power:GND #PWR06
U 1 1 5E1352EA
P 5250 5600
F 0 "#PWR06" H 5250 5350 50  0001 C CNN
F 1 "GND" H 5255 5427 50  0000 C CNN
F 2 "" H 5250 5600 50  0001 C CNN
F 3 "" H 5250 5600 50  0001 C CNN
	1    5250 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4200 4800 4700
Connection ~ 4800 4200
Wire Wire Line
	4800 4700 4800 5200
Connection ~ 4800 4700
Wire Wire Line
	5250 4300 5250 4800
Connection ~ 5250 4800
Wire Wire Line
	5250 4800 5250 5300
Connection ~ 5250 5300
Wire Wire Line
	5250 5300 5250 5600
Text Label 4900 4600 0    50   ~ 0
Analog_1
Text Label 4900 5100 0    50   ~ 0
Analog_2
NoConn ~ 11400 3550
NoConn ~ 8750 1450
NoConn ~ 8750 1550
NoConn ~ 8750 1650
NoConn ~ 8750 1750
NoConn ~ 8750 1850
NoConn ~ 8750 1950
NoConn ~ 8750 2050
NoConn ~ 8750 2150
NoConn ~ 8750 2250
NoConn ~ 8750 2350
NoConn ~ 8750 2450
NoConn ~ 8750 2550
NoConn ~ 8750 2650
NoConn ~ 8750 2750
NoConn ~ 8750 2850
NoConn ~ 8750 2950
NoConn ~ 8750 3050
NoConn ~ 8750 3150
Text Label 3350 1500 0    50   ~ 0
30
Text Label 3350 1400 0    50   ~ 0
31
Text Label 3350 1300 0    50   ~ 0
32
Wire Wire Line
	1450 1500 3350 1500
Text Label 3350 1800 0    50   ~ 0
27
Text Label 3350 1700 0    50   ~ 0
28
Text Label 3350 1600 0    50   ~ 0
29
Wire Wire Line
	1450 1800 3350 1800
Text Label 3350 2200 0    50   ~ 0
23
Wire Wire Line
	1450 2100 3350 2100
Text Label 3350 1900 0    50   ~ 0
26
Text Label 3350 2000 0    50   ~ 0
25
Text Label 3350 2100 0    50   ~ 0
24
Wire Wire Line
	1450 1300 3350 1300
Wire Wire Line
	1450 1400 3350 1400
Wire Wire Line
	1450 1600 3350 1600
Wire Wire Line
	1450 1700 3350 1700
Wire Wire Line
	1450 1900 3350 1900
Wire Wire Line
	1450 2000 3350 2000
Wire Wire Line
	1450 2200 3350 2200
Wire Wire Line
	1450 2300 3400 2300
Wire Wire Line
	1450 2400 3400 2400
Wire Wire Line
	1450 2500 3400 2500
Wire Wire Line
	1450 2600 3400 2600
Wire Wire Line
	1450 2700 3400 2700
Wire Wire Line
	1450 2800 3400 2800
Wire Wire Line
	1450 2900 3400 2900
Wire Wire Line
	1450 3000 3400 3000
Wire Wire Line
	1450 3100 3400 3100
Wire Wire Line
	1450 3200 3400 3200
Text Label 3350 3300 0    50   ~ 0
8
Wire Wire Line
	1450 3300 3350 3300
Text Label 5050 1400 0    50   ~ 0
TX
Wire Wire Line
	4600 1400 5050 1400
Text Label 3350 1200 0    50   ~ 0
34
Text Label 3350 3400 0    50   ~ 0
3
Text Label 3350 3500 0    50   ~ 0
2
$Comp
L Connector:Conn_01x24_Male J6
U 1 1 5E35F84D
P 1250 2300
F 0 "J6" H 1358 3581 50  0000 C CNN
F 1 "Conn_01x24_Male" H 1358 3490 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x24_P2.54mm_Vertical" H 1250 2300 50  0001 C CNN
F 3 "~" H 1250 2300 50  0001 C CNN
	1    1250 2300
	1    0    0    -1  
$EndComp
NoConn ~ 8750 3350
NoConn ~ 8750 3250
$Comp
L teensy:Teensy4.0 U2
U 1 1 5E4B550D
P 7650 2800
F 0 "U2" H 7650 4415 50  0000 C CNN
F 1 "Teensy4.0" H 7650 4324 50  0000 C CNN
F 2 "Teensy:Teensy40" H 7250 3000 50  0001 C CNN
F 3 "" H 7250 3000 50  0001 C CNN
	1    7650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 3750 9150 3750
Wire Wire Line
	8750 3850 9000 3850
Wire Wire Line
	8750 3650 9000 3650
$Comp
L power:+5V #PWR07
U 1 1 5E119F78
P 9150 3750
F 0 "#PWR07" H 9150 3600 50  0001 C CNN
F 1 "+5V" V 9165 3878 50  0000 L CNN
F 2 "" H 9150 3750 50  0001 C CNN
F 3 "" H 9150 3750 50  0001 C CNN
	1    9150 3750
	0    1    1    0   
$EndComp
Text Label 9000 3650 0    50   ~ 0
34
Text Label 9000 3850 0    50   ~ 0
32
Wire Wire Line
	1450 1200 3350 1200
Wire Wire Line
	1450 3400 3350 3400
Wire Wire Line
	1450 3500 3350 3500
$Comp
L Connector:Conn_01x10_Male J7
U 1 1 5E3C8E05
P 2550 4225
F 0 "J7" H 2658 4806 50  0000 C CNN
F 1 "Conn_01x10_Male" H 2658 4715 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 2550 4225 50  0001 C CNN
F 3 "~" H 2550 4225 50  0001 C CNN
	1    2550 4225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Male J8
U 1 1 5E3CA369
P 2550 5425
F 0 "J8" H 2658 6006 50  0000 C CNN
F 1 "Conn_01x10_Male" H 2658 5915 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 2550 5425 50  0001 C CNN
F 3 "~" H 2550 5425 50  0001 C CNN
	1    2550 5425
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5E3CACA9
P 3350 4250
F 0 "#PWR010" H 3350 4000 50  0001 C CNN
F 1 "GND" V 3355 4122 50  0000 R CNN
F 2 "" H 3350 4250 50  0001 C CNN
F 3 "" H 3350 4250 50  0001 C CNN
	1    3350 4250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2750 3825 3350 3825
Wire Wire Line
	3350 3825 3350 3925
Wire Wire Line
	2750 3925 3350 3925
Connection ~ 3350 3925
Wire Wire Line
	2750 4025 3350 4025
Wire Wire Line
	3350 3925 3350 4025
Connection ~ 3350 4025
Wire Wire Line
	3350 4025 3350 4125
Wire Wire Line
	2750 4125 3350 4125
Connection ~ 3350 4125
Wire Wire Line
	3350 4125 3350 4225
Wire Wire Line
	2750 4225 3350 4225
Connection ~ 3350 4225
Wire Wire Line
	3350 4225 3350 4250
Wire Wire Line
	2750 4325 3350 4325
Wire Wire Line
	3350 4325 3350 4250
Connection ~ 3350 4250
Wire Wire Line
	2750 4425 3350 4425
Wire Wire Line
	3350 4425 3350 4325
Connection ~ 3350 4325
Wire Wire Line
	2750 4525 3350 4525
Wire Wire Line
	3350 4525 3350 4425
Connection ~ 3350 4425
Wire Wire Line
	2750 4625 3350 4625
Wire Wire Line
	3350 4625 3350 4525
Connection ~ 3350 4525
Wire Wire Line
	2750 4725 3350 4725
Wire Wire Line
	3350 4725 3350 4625
Connection ~ 3350 4625
$Comp
L power:+5V #PWR011
U 1 1 5E404AE5
P 3350 5425
F 0 "#PWR011" H 3350 5275 50  0001 C CNN
F 1 "+5V" V 3365 5553 50  0000 L CNN
F 2 "" H 3350 5425 50  0001 C CNN
F 3 "" H 3350 5425 50  0001 C CNN
	1    3350 5425
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 5025 3350 5025
Wire Wire Line
	3350 5025 3350 5125
Wire Wire Line
	2750 5125 3350 5125
Connection ~ 3350 5125
Wire Wire Line
	3350 5125 3350 5225
Wire Wire Line
	2750 5225 3350 5225
Connection ~ 3350 5225
Wire Wire Line
	3350 5225 3350 5325
Wire Wire Line
	2750 5325 3350 5325
Connection ~ 3350 5325
Wire Wire Line
	3350 5325 3350 5425
Wire Wire Line
	2750 5425 3350 5425
Connection ~ 3350 5425
Wire Wire Line
	2750 5525 3350 5525
Wire Wire Line
	3350 5525 3350 5425
Wire Wire Line
	2750 5625 3350 5625
Wire Wire Line
	3350 5625 3350 5525
Connection ~ 3350 5525
Wire Wire Line
	2750 5725 3350 5725
Connection ~ 3350 5625
Wire Wire Line
	2750 5825 3350 5825
Wire Wire Line
	3350 5625 3350 5725
Connection ~ 3350 5725
Wire Wire Line
	3350 5725 3350 5825
Wire Wire Line
	2750 5925 3350 5925
Wire Wire Line
	3350 5925 3350 5825
Connection ~ 3350 5825
$EndSCHEMATC
