#define cell1 14
#define cell2 15
#define cell3 16
#define cell4 17

#define relay_all 7
#define relay_odrv 21

#define current_all 22
#define current_odrv 23

#define current_odrv_offset 187
#define current_all_offset 191

#define blue_button 3
#define red_button 4
#define white_button 5
#define ui_switch 6
#define knob 20

#define average_length 10
#define NO_CELLS 4

#define __MK20DX128__

#include <ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Float32.h>

ros::NodeHandle nh;

std_msgs::Float32MultiArray ros_cell_values;
std_msgs::Float32 ros_total_current;
std_msgs::Float32 ros_odrive_current;

ros::Publisher pub_ros_cell_values("/battery/cell_voltages", &ros_cell_values);
ros::Publisher pub_ros_total_current("/battery/total_current", &ros_total_current);
ros::Publisher pub_ros_odrive_current("/battery/odrive_current", &ros_odrive_current);

float cell_calibration_factor[4] = {1.035, 1.025, 1.03, 1.02};

float cell_values[NO_CELLS][average_length];

int rotateLeft(float *array, int n)
{
        float temp = array[0]; // this value stores the first element of the array

        for(int i = 0; i < n-1; i++)
                array[i] = array[i+1]; //counts through the array

        array[n-1] = temp; //swaps the last element in the array with the temp value

        return 1;
}

void setup() {
  // Serial.begin(57600);
  
  pinMode(relay_all, OUTPUT);
  pinMode(relay_odrv, OUTPUT);
  
  // analogReadResolution(10);

  activate_all();
  activate_odrv();

  for(int i = 0; i < NO_CELLS; i++) {
    for(int j = 0; j < average_length; j++) {
      cell_values[i][j] = 3.7;
    }
  }  

  ros_cell_values.data_length = NO_CELLS;
  nh.advertise(pub_ros_cell_values);
  nh.advertise(pub_ros_total_current);
  nh.advertise(pub_ros_odrive_current);
}

void loop() {
  delay(10);
  
  // Serial.println(String(cellv(0)) + "v " + String(cellv(1)) + "v " + String(cellv(2)) + "v " + String(cellv(3)) + "v");

  for(int i = 0; i < NO_CELLS; i++) {
    ros_cell_values.data[i] = cellv(i);
  }

  ros_total_current.data = curr_all();
  ros_odrive_current.data = curr_odrv();
  
  pub_ros_cell_values.publish(&ros_cell_values);
  pub_ros_total_current.publish(&ros_total_current);
  pub_ros_odrive_current.publish(&ros_odrive_current);
  
  nh.spinOnce();
}

float curr_all() {
  return (analogRead(current_all) - current_all_offset) * (3.3 / 1024) * (1/0.06);
}

float curr_odrv() {
  return (analogRead(current_odrv) - current_odrv_offset) * (3.3 / 1024) * (1/0.06);
}

float cellv(int cell) {
  float cell_value = (3.3 / 1024) * analogRead(cell1 + cell) * cell_calibration_factor[cell] * 3 / 2;
  rotateLeft(cell_values[cell], average_length);
  cell_values[cell][average_length - 1] = cell_value;
  float total = 0;
  
  for(int j = 0; j < average_length; j++) {
    total += cell_values[cell][j];
  }
  
  return total / average_length;
}

void activate_all() {
  digitalWrite(relay_all, HIGH);
}

void deactivate_all() {
  digitalWrite(relay_all, LOW);
}

void activate_odrv() {
  digitalWrite(relay_odrv, HIGH);
}

void deactivate_odrv() {
  digitalWrite(relay_odrv, LOW);
}
