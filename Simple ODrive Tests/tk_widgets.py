import tkinter as tk
import time


class HoldButton(tk.Button):
    def __init__(self, master, ondown=None, onup=None, key=None, tooltip=None, debounce=False, **kwargs):
        super().__init__(master, **kwargs)
        self.bind('<Button-1>', ondown)
        self.bind('<ButtonRelease-1>', onup)
        self.down = False
        self.last_down = 0
        self.debounce = debounce

        if key is not None:
            print("Binding", key)
            master.nametowidget(self.winfo_toplevel()).bind("<KeyPress-{}>".format(key), self.sim_down)
            master.nametowidget(self.winfo_toplevel()).bind("<KeyRelease-{}>".format(key), self.sim_up)
            print()

        if tooltip is not None:
            CreateToolTip(self, tooltip)

    def sim_down(self, e):
        self.last_down = time.time()
        if not self.down:
            self.down = True
            self.event_generate("<Button-1>")

    def sim_up(self, e):
        if self.debounce:
            self.after(200, self.check_last_down)
        else:
            self.down = False
            self.event_generate("<ButtonRelease-1>")

    def check_last_down(self):
        if self.down and time.time() - self.last_down > 0.1:
            self.down = False
            self.event_generate("<ButtonRelease-1>")


class CreateToolTip(object):
    """
    create a tooltip for a given widget
    """
    def __init__(self, widget, text='widget info'):
        self.waittime = 500     #miliseconds
        self.wraplength = 180   #pixels
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.widget.bind("<ButtonPress>", self.leave)
        self.id = None
        self.tw = None

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.showtip)

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def showtip(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                       background="#ffffff", relief='solid', borderwidth=1,
                       wraplength = self.wraplength)
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tw
        self.tw= None
        if tw:
            tw.destroy()